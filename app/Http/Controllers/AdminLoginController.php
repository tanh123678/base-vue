<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\LoginRequest;
use Auth;
use App\User;

class AdminLoginController extends Controller
{
    public function getLogin()
    {
        if (Auth::check()) {
            // nếu đăng nhập thàng công thì
            return redirect('admincp');
        } else {
            return view('auth.newlogin');
        }
    }

    public function postLogin(LoginRequest $request)
    {
        $login = [
            'email'    => $request->Email,
            'password' => $request->Password,
            'level'    => 1,
            'status'   => 1
        ];
        if (Auth::attempt($login)) {
            return view('admincp');
        } else {
            return back()->with('status', 'Email hoặc Password không chính xác');
        }
    }

    public function register(Request $request){
        $regis = new User;
        $regis->name = $request->Name;
        $regis->email = $request->Email;
        $regis->password = bcrypt($request->Password);
        $regis->level = 1;
        $regis->status = 1;
        $regis->save();

        $login = [
            'email'    => $regis->email,
            'password' => $regis->password,
            'level'    => 1,
            'status'   => 1
        ];
        if (Auth::attempt($login)) {
            return redirect('admincp');
        }
    }

    public function getLogout()
    {
        Auth::logout();
        return redirect()->route('getLogin');
    }
}
