$(document).ready(function () {
    var x = $('#login'), y = $('#register'), z = $('#btn');

    $('#regis-path').click(function () {
        x.css('left', '-400px');
        y.css('left', '50px');
        z.css('left', '110px')
    })
    $('#login-path').click(function () {
        x.css('left', '50px');
        y.css('left', '450px');
        z.css('left', '0px')
    })


    $('#log-btn').click(function (e) {
        let mail = $('.login-mail').val();
        let pass = $('.login-pass').val();
        // e.preventDefault();
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: '/postLogin',
            type: 'POST',
            data: {
                Email: mail,
                Password: pass
            }
        }).done(function() {

        });
    })

    $('#reg-btn').click(function (e) {
        // e.preventDefault();
        let name = $('.regis-name').val();
        let mail = $('.regis-mail').val();
        let pass = $('.regis-pass').val();
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: '/register',
            type: 'POST',
            data: {
                Name: name,
                Email: mail,
                Password: pass
            }
        }).done(function() {

        });
    })
})
