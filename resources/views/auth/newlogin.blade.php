@extends('layouts.app')

@section('content')
    <div class="hero">
        <div class="form-box">
            <div class="button-box">
                <div id="btn"></div>
                <button type="button" class="toggle-btn" id="login-path">
                    log in
                </button>
                <button type="button" class="toggle-btn" id="regis-path">
                    register
                </button>
            </div>
            @if (count($errors) >0)
                <ul class="mb-0">
                    @foreach($errors->all() as $error)
                        <li style="text-align: center" class="text-danger"> {{ $error }}</li>
                    @endforeach
                </ul>
            @endif

            @if (session('status'))
                <ul class="mb-0">
                    <li style="text-align: center" class="text-danger"> {{ session('status') }}</li>
                </ul>
            @endif
            <form id="login" class="input-group">
                <input type="text" class="input-field login-mail" placeholder="enter Email" required>
                <input type="password" class="input-field login-pass" placeholder="enter Password" required>
{{--                <div style="margin-top: 15px">--}}
{{--                    <input type="checkbox" class="chech-box"><span>remember password</span>--}}
{{--                </div>--}}
                <button type="submit" class="submit-btn" id="log-btn" style="margin-top: 35px;">log in</button>
            </form>
            <form id="register" class="input-group">
                <input type="text" class="input-field regis-name" placeholder="enter Name" required>
                <input type="email" class="input-field regis-mail" placeholder="enter Email" required>
                <input type="password" class="input-field regis-pass" placeholder="enter Password" required>
{{--                <div style="margin-top: 15px">--}}
{{--                    <input type="checkbox" class="chech-box"><span>agree to the fuckin condition</span>--}}
{{--                </div>--}}
                <button type="submit" class="submit-btn" id="reg-btn">regis and login</button>
            </form>
        </div>
    </div>
@endsection
