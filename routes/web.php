<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});
//
//Auth::routes();

//Route::get('/home', 'HomeController@index')->name('home');

Route::get('login','AdminLoginController@getLogin');
Route::post('postLogin','AdminLoginController@postLogin');
Route::post('register', 'AdminLoginController@register');
Route::post('logout','AdminLoginController@getLogout');

Route::group(['middleware' => 'checkAdminLogin', 'prefix' => 'admincp'], function() {
    Route::get('/', function() {
        return view('home');
    });
});

